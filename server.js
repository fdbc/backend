var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

  //variable para poder usar el request-json

  var requestJson = require('request-json');

  //variable para que se pueda parsear el req.body y no salga como undefined

  var bodyParser = require('body-parser');

  app.use(bodyParser.json()); // for parsing application/json

  var path = require('path');

  var autenticate = "https://private-anon-7b33940e93-apsandbox.apiary-mock.com/api/authentication/";

  var accounts = "https://private-anon-7b33940e93-apsandbox.apiary-mock.com/api/accounts/"
  //se puede invocar al metodo accounts pasandole el parametro
  //?accountids=account-0%3Baccount-1
  var transactions = "https://private-ee1902-apsandbox.apiary-mock.com/api/transactions/"

  var assets = "https://private-ee1902-apsandbox.apiary-mock.com/api/assets/"
  //se puede invocar al metodo assets pasandole el parametro
  //?assetids=asset-0%3Basset-1

  var loans = "https://private-ee1902-apsandbox.apiary-mock.com/api/loans/"
  //se puede invocar al metodo assets pasandole el parametro
  //?loanids=loan-0%3Bloan-1


app.listen(port);

console.log('todo list RESTful API server started on: ' + port);


app.post('/login',function(req,res){
  var user_name=req.body.user;
  var password=req.body.password;
  console.log("User name = "+user_name+", password is "+password);
  res.end("yes");
});

app.get('/accounts',function(req,res) {
  var clienteAPI = requestJson.createClient(accounts);
  clienteAPI.get('',function(err,resM,body) {
    if(err)
    {
      console.log(body);
    }
    else
    {
      res.send(body);
    }
  }
)});
app.get('/transactions',function(req,res) {
  var clienteAPI = requestJson.createClient(transactions);
  clienteAPI.get('',function(err,resM,body) {
    if(err)
    {
      console.log(body);
    }
    else
    {
      res.send(body);
    }
  }
)});
app.get('/assets',function(req,res) {
  var clienteAPI = requestJson.createClient(assets);
  clienteAPI.get('',function(err,resM,body) {
    if(err)
    {
      console.log(body);
    }
    else
    {
      res.send(body);
    }
  }
)});
app.get('/loans',function(req,res) {
  var clienteAPI = requestJson.createClient(loans);
  clienteAPI.get('',function(err,resM,body) {
    if(err)
    {
      console.log(body);
    }
    else
    {
      res.send(body);
    }
  }
)});
